<?php
/**
 * Plugin Name:     Ptchr Instagram Feed
 * Plugin URI:      https://ptchr.nl
 * Description:     Insert a instagram feed on your website
 * Author:          Ptchr
 * Author URI:      https://ptchr.nl
 * Text Domain:     igfeed
 * Domain Path:     /languages
 * Version:         0.2.2
 *
 * @package         Igfeed
 */
require plugin_dir_path(__FILE__) . 'vendor/autoload.php';

if (!defined('ABSPATH')) {
	return;
}

use Pitcher\Instagram\Client;
use Pitcher\Instagram\Cronjob;
use Pitcher\Instagram\Listener;
use Pitcher\Instagram\Notice;
use Pitcher\Instagram\Settings;
use Pitcher\Instagram\Shortcodes;


class PtchrInstagram
{

	public $version = "0.2.2";

	public function __construct()
	{
		$this->client = new Client();

		new Listener($this->client);

		new Notice($this->client);

		new Cronjob($this->client);

		new Shortcodes();

		if (is_admin()) {
			new Settings();
		}

	}

}

new PtchrInstagram();
