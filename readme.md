=== Ptchr Instagram Feed ===
Contributors: Ptchr
Donate link: https://ptchr.nl
Tags: instagram

Instagram feed connector including Auth cron.

== Description ==

Plugin with php function to retrieve latest instagram posts. Used for showing a feed of the connected account.

== Installation ==

1. install this plugin
2. Create a Facebook App that uses the Instagram Basic Display Product
3. Retrieve the instagram app-id and the instagram app secret (via app->products->Instagram basic Display -> Basic Display)
4. Add the url of your website to the Valid OAuth Redirect URIs in (via app->products->Instagram basic Display -> Basic Display)
5. fill in the instagram app id and the app secret in settings->Ptchr Instagram. Leave the long lived token empty. This will be filled automatically ( expiry is optional ).
6. After entering the app credentials a link will appear at the top of the screen, this link redirects to Instagram where you can login with your username and password
7. If the login is successful the plugin is succesfully set up.


== What about Crons? ==

The plugin runs a cronjob every day via WordPress to check if the retrieved token should be extended. This is an automatic process.
If The cronjob is not active the connection with Instagram will expire after a few weeks.

If you have a website that is not often visited you should consider using a manual PHP cron to periodically run the wordpress cron as such:
```
$ php /path/to/wordpress/wp-cron.php
```

If the cronjob has run, a datetime stamp will be stored and is then visible in the settings->Ptchr Instagram page.

== Changelog ==

= 0.2 =
Rewrite, native wordpress functions added.
