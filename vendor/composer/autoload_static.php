<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInitc095974856e272ab5609c844e2f6adc0
{
    public static $prefixLengthsPsr4 = array (
        'P' => 
        array (
            'Pitcher\\Instagram\\' => 18,
        ),
        'E' => 
        array (
            'EspressoDev\\InstagramBasicDisplay\\' => 34,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'Pitcher\\Instagram\\' => 
        array (
            0 => __DIR__ . '/../..' . '/app',
        ),
        'EspressoDev\\InstagramBasicDisplay\\' => 
        array (
            0 => __DIR__ . '/..' . '/espresso-dev/instagram-basic-display-php/src',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInitc095974856e272ab5609c844e2f6adc0::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInitc095974856e272ab5609c844e2f6adc0::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
