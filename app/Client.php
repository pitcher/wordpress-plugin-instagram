<?php

namespace Pitcher\Instagram;

use \EspressoDev\InstagramBasicDisplay\InstagramBasicDisplay;


/**
 * Class Client
 * @package App\Controllers\Instagram
 * Wrapper Class that Converts .env variables into InstagramClass.
 */
class Client
{
	private $cachePrefix = "instagramApi_";

	public function __construct()
	{
		$this->appId = Settings::getAppId() ?? false;
		$this->appSecret = Settings::getAppSecret() ?? false;
		$this->redirectUri = home_url() . "/";
		$this->expiration = Settings::getExpiriration();
		$this->longLivedToken = Settings::getLongLivedToken() ?? false;
		$this->code = $this->getCodeFromCache();

		// Display debugger. Remove later
		if (is_admin() && $_GET['ptchrdebugger']) {
			add_action('admin_notices', array($this, 'debugger'));
		}
	}

	function auth()
	{
		// Get instagramBasicDisplay Instance
		$instagram = $this->InstagramBasicDisplay();

		// Get the short lived access token (valid for 1 hour)
		$oAuthToken = $instagram->getOAuthToken($this->getCodeFromCache(), true);

		// Exchange this token for a long lived token (valid for 60 days)
		$longLivedToken = $instagram->getLongLivedToken($oAuthToken, true);

		// Set LongLivedToken
		Settings::setLongLivedToken($longLivedToken);
	}


	function InstagramBasicDisplay()
	{
		$instagram = new InstagramBasicDisplay([
			'appId' => $this->appId,
			'appSecret' => $this->appSecret,
			'redirectUri' => $this->redirectUri
		]);

		return $instagram;
	}

	function extendLongLivedAuthToken()
	{
		if (!Settings::getLongLivedToken()) {
			return false;
		}

		$instagram = $this->InstagramBasicDisplay();

		$refreshedtoken = $instagram->refreshToken(Settings::getLongLivedToken(), true);

		if ($refreshedtoken) {
			Settings::setLongLivedToken($refreshedtoken);

			// store date for last refreshed token for debugging purposes
			set_transient('lastRefreshedTokenDate', date('l jS \of F Y h:i:s A'));
		}

		if (!$refreshedtoken) {
			Settings::setLongLivedToken(false);
		}

		return true;
	}


	function getCodeFromCache()
	{
		$transient_key = $this->cachePrefix . "_code";
		if (false === ($data = get_transient($transient_key))) {
			return false;
		}
		return $data;
	}

	function setCodeInCache($code)
	{
		$expiration = 60; // 1 minute;
		$transient_key = $this->cachePrefix . "_code";
		set_transient($transient_key, $code, $expiration);
	}

	function getLoginUrl()
	{
		$instagram = $this->InstagramBasicDisplay();
		return $instagram->getLoginUrl();
	}

	function debugger()
	{ ?>
		<div class="notice notice-alert">
           <pre>
               <?php var_dump($this); ?>
           </pre>
		</div>
		<?php
	}

}
