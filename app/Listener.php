<?php


namespace Pitcher\Instagram;


class Listener
{
	public function __construct(Client $client)
	{
		add_action('wp_loaded', function () use ($client) {

			// only open listener when the longlivedcode is not set
			if (!Settings::getLongLivedToken()
				&& isset($_GET['code'])) {
				$client->setCodeInCache(sanitize_text_field($_GET['code']));
				$client->auth();

				if(Settings::getLongLivedToken()){
					echo "<div style='width:100%; background-color:rgba(0,255,0,0.1); text-align:center;padding:3em;'>PtchrInstagram: <span style='font-weight:bold'>Connection setup completed.</span></div>";
				}else{
					echo "<div style='width:100%; background-color:rgba(255,0,0,0.1); text-align:center;padding:3em;'>PtchrInstagram: <span style='font-weight:bold'>Connection setup failed</span></br> <small>longLived Token could not be set. <a href='/wp-admin/options-general.php?page=ptchr-instagram'>Please verify if App ID and App Secret are correctly setup.</a></small></div>";
				}

			}

		});


		add_action('wp_loaded', function () use ($client) {
			if (is_admin()
				&& Settings::getLongLivedToken()
				&& isset($_GET['renew'])) {
				$client->extendLongLivedAuthToken();
				var_dump('renew');
				die();

			}
		});

	}

}
