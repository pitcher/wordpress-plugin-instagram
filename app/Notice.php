<?php


namespace Pitcher\Instagram;


class Notice
{
	public function __construct($client)
	{

		if (!Settings::getLongLivedToken()) {
			add_action('admin_notices', function () use ($client) {
				?>
				<div class="notice notice-alert">
					<p>Zo te zien is de koppeling met Instagram verlopen, log hier opnieuw in om de laatste Instagram
						Posts weer
						te tonen op je website.</p>
					<p>Log in met het Instagram account waarvan je de posts op de site wil tonen.</p>
					<a href="<?php echo $client->getLoginUrl() ?>">Klik hier om je Instagram account te koppelen</a>
				</div>
				<?php
			});
		}

	}
}
