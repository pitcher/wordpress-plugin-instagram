<?php


namespace Pitcher\Instagram;

use EspressoDev\InstagramBasicDisplay\InstagramBasicDisplay;
use EspressoDev\InstagramBasicDisplay\InstagramBasicDisplayException;

class Controller
{
	static $cachePrefix = "instagramApi_";

	/**
	 * @throws InstagramBasicDisplayException
	 */
	static function getLatestPosts(): array
	{
		if (!Settings::getLongLivedToken()) {
			return [];
		}

		$transient_key = self::$cachePrefix . "_getLatestPosts";
		if (false === ($items = get_transient($transient_key))) {
			$instagram = New InstagramBasicDisplay(Settings::getLongLivedToken());

			$items = $instagram->getUserMedia();

			set_transient($transient_key, $items, Settings::getExpiriration());
			if (isset($items->data) && count($items->data) > 0) {
				return $items->data;
			} else {
				return [];
			}
		}
		if (isset($items->data) && count($items->data) > 0) {
			return $items->data;
		} else {
			return [];
		}
	}
}
