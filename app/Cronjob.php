<?php

namespace Pitcher\Instagram;


class Cronjob
{

	private $cronperiod = "daily";

	public function __construct(Client $client)
	{
		wp_schedule_event(strtotime('00:00'), $this->cronperiod, 'instagramCronJob');

		add_action('instagramCronJob', function () use ($client) {
			$client->extendLongLivedAuthToken();
		});
	}
}
