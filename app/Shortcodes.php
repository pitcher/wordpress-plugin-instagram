<?php


namespace Pitcher\Instagram;

class Shortcodes
{
	public function __construct()
	{

		// Todo: shortcode for proper display
		add_shortcode('pitcher_instagram_posts', function () {
			return json_encode(Controller::getLatestPosts());
		});
	}
}
